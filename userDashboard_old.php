<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="User Dashboard | " />
    <title>User Dashboard | </title>
    <!-- <title>User Dashboard | De Xin Guo Ji 德鑫国际</title> -->
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
   	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _USERDASHBOARD_DASHBOARD ?> - <?php echo _USERDASHBOARD_DOWNLINE ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="left-padding-div">
        <?php
        $conn = connDB();
        if($getWho)
        {
            echo '<ul>';
            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                if($thisPerson->getCurrentLevel() == $lowestLevel)
                {
                    echo '<li id="'.$thisPerson->getReferralId().'"><img src="img/tiara1.png" class="tiara-png"><b>'.$thisTempUser->getUsername().'</b>&nbsp;&nbsp;('.$thisTempUser->getRank().'&nbsp;&nbsp;'.$thisTempUser->getStatus().')</li>';
                }
            }
            echo '<ul>';
            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                echo '
                <script type="text/javascript">
                var div = document.getElementById("'.$thisPerson->getReferrerId().'");
                div.innerHTML += "<ul  class=\'tiara-ul2\' name=\'ul-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'><img src=\'img/tiara2.png\' class=\'tiara-png\'><b>'.$thisTempUser->getUsername().'</b>&nbsp;&nbsp;('.$thisTempUser->getRank().'&nbsp;&nbsp;'.$thisTempUser->getStatus().')</li></ul>";
                </script>
                ';
            }
            echo '</ul>';
        }
        ?>
	</div>
</div>
	<div class="clear"></div>
    <img src="img/female.png" alt="<?php echo _USERDASHBOARD_FEMALE ?>" title="<?php echo _USERDASHBOARD_FEMALE ?>" class="female-png">
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>    

<?php include 'js.php'; ?>
</body>
</html>
