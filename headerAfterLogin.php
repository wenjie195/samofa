<?php
if(isset($_SESSION['uid']))
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right logged-in-menu">
            	<a href="index.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _HEADERBEFORELOGIN_HOME ?>
                </a>              
            	<a href="userDashboard.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _USERDASHBOARD_DASHBOARD ?>
                </a>              
            	<a href="register.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _HEADERBEFORELOGIN_REGISTER ?>
                </a>   
            	<a href="productDetails.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _HEADER_ORDER ?>
                </a>                          
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                	</div>
                </div>  
            	<a href="logout.php" class="pink-hover-text menu-item">
                	<?php echo _MAINJS_ALL_LOGOUT ?>
                </a>
                           	<div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="index.php"><?php echo _HEADERBEFORELOGIN_HOME ?></a></li>
                                  <li><a href="userDashboard.php"><?php echo _USERDASHBOARD_DASHBOARD ?></a></li>
                                  <li><a href="register.php"><?php echo _HEADERBEFORELOGIN_REGISTER ?></a></li>
                                  <li><a href="productDetails.php"><?php echo _HEADER_ORDER ?></a></li>                                
                                  <li><a href="<?php $link ?>?lang=en">Swicth to English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>
                                  <li><a href="logout.php" ><?php echo _MAINJS_ALL_LOGOUT ?></a></li>

                                </ul>
							</div><!-- /dl-menuwrapper -->                                         	
            </div>
        </div>

</header>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/samofa.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>



            <div class="right-menu-div float-right before-header-div">
           
            	<a class="pink-hover-text menu-margin-right menu-item open-login">
                	<?php echo _HEADERBEFORELOGIN_LOGIN ?>
                </a>  
            	<a href="index.php" class="pink-hover-text menu-margin-right menu-item">
                	<?php echo _HEADERBEFORELOGIN_HOME ?>
                </a>                  
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect">
                    
                    <?php echo _HEADERBEFORELOGIN_PRODUCT ?>
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADERBEFORELOGIN_PRODUCT ?>" title="<?php echo _HEADERBEFORELOGIN_PRODUCT ?>">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADERBEFORELOGIN_PRODUCT ?>" title="<?php echo _HEADERBEFORELOGIN_PRODUCT ?>">
                                
                	<div class="dropdown-content yellow-dropdown-content menu-item">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="magicBloca.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Magic BloCA<sup class="pink-hover-text">TM</sup> 脱糖宝</a></p>
                        <p class="dropdown-p"><a href="hairSerum.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _INDEX_HAIR_SERUM_SHORT ?></a></p>
                	</div>
                </div>                  
                        
                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect">
                    
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="Language / 语言" title="Language / 语言">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="Language / 语言" title="Language / 语言">
                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content menu-item">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                	</div>
                </div>  
                           	<div id="dl-menu" class="dl-menuwrapper before-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a class="open-login"><?php echo _HEADERBEFORELOGIN_LOGIN ?></a></li>                                
                                  <li><a href="index.php"><?php echo _HEADERBEFORELOGIN_HOME ?></a></li>
                                  <li><a href="magicBloca.php">Magic BloCA 脱糖宝</a></li>  
                                  <li><a href="hairSerum.php"><?php echo _INDEX_HAIR_SERUM_SHORT ?></a></li>                              
                                  <li><a href="<?php $link ?>?lang=en">Swicth to English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>

                                </ul>
							</div><!-- /dl-menuwrapper -->  
                                       	
            </div>
        </div>

</header>

<?php
}
?>