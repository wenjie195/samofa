<?php
//Start the session
if (session_id() == ""){
    session_start();
}
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    // header('Location:../index.php?lang=ch');
    exit();
}
else {
    $uid = $_SESSION['uid'];
}
if (isset($_GET['lang']) == 'en') {
  $_SESSION['lang'] = 'en';
}elseif (isset($_GET['lang']) == 'ch') {
  $_SESSION['lang'] = 'ch';
}
