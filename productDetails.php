<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productDetails = getProduct($conn, "WHERE display='1' AND type='1' ");
// $productDetails = getProduct($conn, "WHERE display='1'  ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="Product Details | Samofa 莎魔髪" />
    <title>Product Details | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _PRODUCTDETAILS ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
    <!-- <form action="utilities/productOrderFunction.php" method="POST"> -->
    <form action="#" method="POST">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _PRODUCTDETAILS_IMAGE ?></th>
                    <th><?php echo _PRODUCTDETAILS_PRICE ?> (RM)</th>
                    <th><?php echo _PRODUCTDETAILS_STOCK ?></th>
                    <th><?php echo _PRODUCTDETAILS_STATUS ?></th>
                    <th><?php echo _PRODUCTDETAILS_DESCRIPTION ?></th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td><?php echo ($cnt+1)?></td>
                        <td><?php echo $productDetails[$cnt]->getName();?></td>
                        <td>
                            <img src="img/magic-bloca-product.png" class="product-img" alt="<?php echo $productDetails[$cnt]->getName();?>" title="<?php echo $productDetails[$cnt]->getName();?>">
                            <?php $productDetails[$cnt]->getImages();?>
                        </td>
                        <td><?php echo $productDetails[$cnt]->getPrice();?></td>
                        <td><?php echo $productDetails[$cnt]->getStock();?></td>
                        <td><?php echo _PRODUCTDETAILS_HOT_TOP_SALES ?></td>
                        <td><?php echo $productDetails[$cnt]->getDescription();?></td>

                        <td>

                            <!-- <div class="input-group">
                                <button type="button" class="button-minus math-button clean">-</button>
                                    <input readonly type="number" step="1" max="10" value="0" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                                <button type="button" class="button-plus math-button clean">+</button>
                            </div> -->

                            <input class="clean de-input table-input"  type="text" placeholder="Please enter the value" id="purchase_amount" name="purchase_amount"> 
                            <input class="clean de-input table-input"  type="hidden" value="<?php //echo $productDetails[$cnt]->getName();?>" id="product_name" name="product_name" readonly>  
                            <input class="clean de-input table-input"  type="hidden" value="<?php //echo $productDetails[$cnt]->getPrice();?>" id="product_price" name="product_price" readonly>  
                        
                            <!-- <input type="text" value="<?php //echo $productDetails[$cnt]->getName();?>" name="product-list-id-input['.$index.']">

                            <div class="input-group">
                                <button type="button" class="button-minus math-button clean">-</button>
                                    <input readonly type="number" step="1" max="10" value="0" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                                <button type="button" class="button-plus math-button clean">+</button>
                            </div> -->
                        
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                ?>
            </tbody>
        </table>

        <div class="width100 text-center top-bottom-distance">
            <button class="clean button-width transparent-button dark-pink-button" name="purchase">Checkout</button>
        </div>

    </form>
    </div>
</div>    

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

<script>
    function addToCart()
    {
        $shoppingCart = array();
        $totalProductCount = count($_POST['product-list-id-input']);
        for($i = 0; $i < $totalProductCount; $i++)
        {
            $productId = $_POST['product-list-id-input'][$i];
            $quantity = $_POST['product-list-quantity-input'][$i];
            $thisOrder = array();
            $thisOrder['productId'] = $productId;
            $thisOrder['quantity'] = $quantity;
            array_push($shoppingCart,$thisOrder);
        }
        if(count($shoppingCart) > 0)
        {
            $_SESSION['shoppingCart'] = $shoppingCart;
        }
    }
</script>

</body>
</html>