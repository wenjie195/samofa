<div class="footer-div">
	<p class="footer-p"><?php echo _JS_FOOTER ?></p>
</div>
<!-- Ingredient Modal -->
<div id="ingredient-modal" class="modal-css">
	<div class="modal-content-css forgot-modal-content login-modal-content signup-modal-content">
        <span class="close-css close-ingredient">&times;</span>
        <div class="clear"></div>
        <h1 class="modal-h1 dark-pink-text text-center"><?php echo _INDEX_MAIN_INGREDIENTS ?></h1>
        <div class="left-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_MIX_FRUIT_POWDER ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_MIX_FRUIT_POWDER_DESC ?></p>
        </div>
        <div class="mid-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_PSYLLIUM_HUSK ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_PSYLLIUM_HUSK_DESC ?></p>
        </div>        
        <div class="right-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_INULIN ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_INULIN_DESC ?></p>
        </div>
        <div class="tempo-three-clear"></div>
        <div class="left-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_FLAVONE ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_FLAVONE_DESC ?></p>
        </div>
        <div class="mid-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_MULTIENZYME ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_MULTIENZYME_DESC ?></p>
        </div>        
        <div class="right-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_KONJAC ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_KONJAC_DESC ?></p>
        </div>        
        <div class="tempo-three-clear"></div>
        <div class="left-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_AMYLASE ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_AMYLASE_DESC ?></p>
        </div>
        <div class="mid-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_MULTIENZYME ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_MULTIENZYME_DESC ?></p>
        </div>        
        <div class="right-ingredient">
        	<h2 class="dark-pink-text ingredient-h2"><?php echo _INDEX_KONJAC ?></h2>
            <p class="ingredient-p mild-pink-text2"><?php echo _INDEX_KONJAC_DESC ?></p>
        </div>         
        
        
        <div class="clear"></div>        
	</div>

</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
        <h1 class="dark-pink-text contact-title  hi-title text-center modal-h1 ow-mbtm10"><?php echo _MAINJS_INDEX_LOGIN ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
        <div class="clear"></div>
        <form action="utilities/loginFunction.php" method="POST">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            <div class="clear"></div>
            <p class="input-top-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
            <input class="clean de-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
            <div class="clear"></div>
            <div class="width100 text-center top-bottom-distance">
            	<button class="clean width100 transparent-button dark-pink-button" name="loginButton"><?php echo _MAINJS_INDEX_LOGIN ?></button>
            </div>
            <div class="clear"></div>
    	</form>
        
                
	</div>

</div>

   
<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
<script>
    var videoPlayer = document.getElementById('videoPlayer');

    // Auto play, half volume.
    videoPlayer.play()
    videoPlayer.volume = 1;

    // Play / pause.
    videoPlayer.addEventListener('click', function () {
        if (videoPlayer.paused == false) {
            videoPlayer.pause();
            videoPlayer.firstChild.nodeValue = 'Play';
        } else {
            videoPlayer.play();
            videoPlayer.firstChild.nodeValue = 'Pause';
        }
    });
</script>
<script>
$("#videoPlayer")[0].autoplay = true;

</script>
<script>

var ingredientmodal = document.getElementById("ingredient-modal");
var loginmodal = document.getElementById("login-modal");

var openingredient = document.getElementsByClassName("open-ingredient")[0];
var openlogin = document.getElementsByClassName("open-login")[0];


var closeingredient = document.getElementsByClassName("close-ingredient")[0];
var closelogin = document.getElementsByClassName("close-login")[0];

if(openingredient){
openingredient.onclick = function() {
  ingredientmodal.style.display = "block";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}

if(closeingredient){
  closeingredient.onclick = function() {
  ingredientmodal.style.display = "none";
}
}
if(closelogin){
  closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == ingredientmodal) {
    ingredientmodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
}
</script>
<script>
$(function(){$(".numbers-row").append('<div class="dec button2 hover1 button hover-effect"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button hover-effect"><span class="display-none">+</span><img src="img/plus.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/plus2.png" class="hover1b add-minus" alt="Add" title="Add"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>
	jQuery(function($) { // DOM ready and $ alias in scope
	
	  /**
	   * Option dropdowns. Slide toggle
	   */
	  $(".option-heading").on('click', function() {
		$(this).toggleClass('is-active').next(".option-content").stop().slideToggle(500);
	  });
	
	});

</script>