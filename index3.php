<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Samofa 莎魔髪" />
    <title>Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerBeforeLogin.php'; ?>
<div class="video-div width100 menu-distance">
  <div id="fb-root"></div>
  <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

  
  <div class="fb-video" data-href="https://www.facebook.com/235643117303835/videos/692348604599652/" data-width="1920" data-show-text="false" class="home-vido" autoplay loop>
  </div>
</div>
<!--
<div class="video-div width100 menu-distance">
	<video class="home-video" autoplay poster="img/samofa-poster.jpg" id="videoPlayer" loop>
    	<source src="img/samofa-video.mp4" type="video/mp4">
    </video>
</div>-->
<div class="width100 same-padding quote-big-div">
	<div class="three-div-width pink-gradient-bg ow-padding">
    	<div class="white-bg ow-width quote-div">	
            <p class="dark-pink-text quote-p">
                <?php echo _INDEX_QUOTE1 ?>
            </p>
            <p class="light-pink-text quote-author">
                <?php echo _INDEX_QUOTE_AUTHOR1 ?>
            </p>
        </div>
    </div>
	<div class="three-div-width mid-three-div-width-margin pink-gradient-bg ow-padding">
    	<div class="white-bg ow-width quote-div">
            <p class="dark-pink-text quote-p">
                <?php echo _INDEX_QUOTE2 ?>
            </p>
            <p class="light-pink-text quote-author">
                <?php echo _INDEX_QUOTE_AUTHOR2 ?>
            </p> 
         </div>   
    </div>    	
	<div class="three-div-width pink-gradient-bg ow-padding">
    	<div class="white-bg ow-width quote-div">
            <p class="dark-pink-text quote-p">
                <?php echo _INDEX_QUOTE3 ?>
            </p>
            <p class="light-pink-text quote-author">
                <?php echo _INDEX_QUOTE_AUTHOR3 ?>
            </p>
        </div>      
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center product-big-div">
	<h1 class="dark-pink-text hi-title contact-title"><?php echo _INDEX_OUR_PRODUCT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="float-left product-img-div">
    <?php echo _INDEX_AMYLASE2 ?>
    	<img src="img/magic-bloca.png" class="width100" alt="Magic BloCA 脱糖宝" title="Magic BloCA 脱糖宝">
    </div>
    <div class="float-right product-desc-div">
    	<h2 class="dark-pink-text product-title">Magic BloCA<sup class="dark-pink-text smaller-h1">TM</sup> 脱糖宝</h2>
        <h3 class="mild-pink-text h3-sub"><i class="mild-pink-text h3-sub"><?php echo _INDEX_MAGIC_DESC ?></i></h3>
        <div class="clear"></div>
        <div class="function-div function-div1">
        	<p class="dark-pink-text title-p"><?php echo _INDEX_FUNCTION ?>:</p>
            <table class="transparent-table mild-pink-text-td">
            <tr>
            	<td><img src="img/tick.png" class="tick-png"></td>
            	<td><?php echo _INDEX_BLOCK_SUGAR ?></td>
            </tr>
            <tr>
            	<td><img src="img/tick.png" class="tick-png"></td>    
                <td><?php echo _INDEX_BLOCK_CARB ?></td>
            </tr>
            <tr>
            	<td><img src="img/tick.png" class="tick-png"></td>                
            	<td><?php echo _INDEX_BLOCK_FAT_BURNING ?></td>
            </tr>
            <tr>
            	<td><img src="img/tick.png" class="tick-png"></td>                
                <td><?php echo _INDEX_BLOCK_DETOX ?></td> 
            </tr>
            <tr>
            	<td><img src="img/tick.png" class="tick-png"></td>                
                <td><?php echo _INDEX_ANTI_HUNGER ?></td>               
            </tr>
            
            </table>
        </div>
        <div class="clear"></div>
        <div class="function-div">
        	<p class="dark-pink-text title-p"><?php echo _INDEX_USE_METHOD ?>:</p>
            <table class="transparent-table mild-pink-text-td">
                <tr>
                    <td><img src="img/drink.png" class="tick-png"></td>
                    <td><?php echo _INDEX_STEP1 ?></td>
                </tr>
                <tr>
                    <td><img src="img/cut.png" class="tick-png"></td>    
                    <td><?php echo _INDEX_STEP2 ?></td>
                </tr>
            </table>
        </div>        
        <div class="clear"></div>
        	<a class="open-ingredient ingrediet-a pink-a"><?php echo _INDEX_INGREDIENTS ?> <img src="img/feather.png" class="smaller-feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></a>
      
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="quantity-p dark-pink-text"><?php echo _INDEX_QUANTITY ?>:</p> <div class="numbers-row numbers-row-css"></div>
        </div>
        <div class="clear"></div>
        <div class="border-btn add-to-cart-btn"><div class="white-bg"><?php echo _INDEX_ADD_TO_CART ?> | RM XXXX</div></div>
    </div>
    <div class="clear"></div>
	<div class="float-right product-img-div extra-margin-top">
    	<img src="img/purifying-balancing-shampoo-serum-ultimate-elixir.png" class="width100" alt="<?php echo _INDEX_HAIR_SERUM ?>" title="<?php echo _INDEX_HAIR_SERUM ?>">
    </div>
    <div class="float-left product-desc-div extra-margin-top extra-margin-top2">
    	<h2 class="dark-pink-text product-title"><?php echo _INDEX_HAIR_SERUM ?> (ML)</h2>
        <h3 class="mild-pink-text h3-sub"><i class="mild-pink-text h3-sub"><?php echo _INDEX_SERUM_DESC ?></i></h3>
    </div>    
    
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center quote-big-div contact-big-div">
	<h1 class="dark-pink-text hi-title contact-title"><?php echo _INDEX_INTERESTED_IN_US ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="three-div-width text-center contact-three-div">
        <a href="tel:+60124616966">
            <img src="img/call.png" alt="<?php echo _INDEX_CONTACT_NO ?>" title="<?php echo _INDEX_CONTACT_NO ?>" class="contact-icon">
            <p class="contact-p dark-pink-text">012-461 6966</p>
        </a>
    </div>
	<div class="three-div-width mid-three-div-width-margin text-center contact-three-div mid-contact-three-div">
    	<img src="img/email.png" alt="<?php echo _INDEX_EMAIL ?>" title="<?php echo _INDEX_EMAIL ?>" class="contact-icon">
        <p class="contact-p dark-pink-text">samofa.official@gmail.com</p>    
    </div>    
	<div class="three-div-width text-center contact-three-div">
    	<a href="https://www.facebook.com/SAMOFA.Official/" class="opacity-hover" target="_blank">
            <img src="img/facebook.png" alt="Facebook" title="Facebook" class="contact-icon">
            <p class="contact-p dark-pink-text">SAMOFA.Official</p>     
        </a>
    </div>    
</div>


<style>
.fb-video iframe {
    margin-top: -220px;
	width: 100%;
}
@media all and (max-width: 1770px){
.fb-video iframe {
	margin-top: -190px;
}	
	
}
@media all and (max-width: 1550px){
.fb-video iframe {
    margin-top: -170px;
}	
}
@media all and (max-width: 1380px){
.fb-video iframe {
    margin-top: -150px;
}		
}
@media all and (max-width: 1230px){
.fb-video iframe {
    margin-top: -130px;
}		
}
@media all and (max-width: 1050px){
.fb-video iframe {
    margin-top: -130px;
}
}
@media all and (max-width: 800px){
.fb-video iframe {
    margin-top: 0;
}
}
</style>
<?php include 'js.php'; ?>
</body>
</html>