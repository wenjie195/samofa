<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];
$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$getWho = getReferralHistory($conn, " WHERE referrer_id =? ", array("referrer_id"), array($uid), "s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="User Dashboard | Samofa 莎魔髪" />
    <title>User Dashboard | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _USERDASHBOARD_DASHBOARD ?> - <?php echo _USERDASHBOARD_DOWNLINE ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _USERDASHBOARD_GEN ?></th>
                    <th><?php echo _USERDASHBOARD_NAME ?></th>
                    <th>ID</th>
                    <th><?php echo _USERDASHBOARD_SPONSOR ?></th>
                    <th><?php echo _USERDASHBOARD_LAST_ORDER ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _JS_COUNTRY ?></th>
                    <th><?php echo _USERDASHBOARD_STATUS ?></th>
                    <th><?php echo _USERDASHBOARD_SALES_VALUE ?></th>
                    <th><?php echo _USERDASHBOARD_HIERARCHY ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($getWho)
                {
                    for($cnt = 0;$cnt < count($getWho) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td><?php echo $getWho[$cnt]->getCurrentLevel();?></td>

                        <td>
                            <?php
                                $userUid = $getWho[$cnt]->getReferralId();

                                $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                                echo $username = $thisUserDetails[0]->getUsername();
                            ?>
                        </td>

                        <td><?php echo $userMemberID = $thisUserDetails[0]->getMemberID();;?></td>

                        <td>
                            <?php
                                $uplineUid = $getWho[$cnt]->getReferrerId();

                                $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                                // $userData = $uplineDetails[0];

                                echo $uplineUsername = $uplineDetails[0]->getUsername();

                            ?>
                        </td>

                        <td><?php echo date("d-m-Y",strtotime($thisUserDetails[0]->getDateUpdated()));?></td>

                        <td><?php echo $rank = $thisUserDetails[0]->getRank();;?></td>
                        <td><?php echo $country = $thisUserDetails[0]->getCountry();;?></td>
                        <td><?php echo $status = $thisUserDetails[0]->getStatus();;?></td>
                        <td><?php echo $salesValue = $thisUserDetails[0]->getSalesValue();;?></td>
                        <!-- <td>Tree View</td> -->

                        <td>
                            <form action="userDownlineDetails.php" method="POST">
                                <button class="clean pink-button view-btn" type="submit" name="user_uid" value="<?php echo $getWho[$cnt]->getReferralId();?>"><?php echo _USERDASHBOARD_VIEW ?>

                                </button>
                            </form>
                        </td>

                    </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>

    <div class="clear"></div>
    <div class="clear"></div>
    <div class="clear"></div>

    <div class="overflow-scroll-div same-padding">
        <!-- <table class="table-css">
            <thead>
                <tr>
                    <th>Generation</th>
                    <th>Name</th>
                    <th>ID</th>
                    <th>Sponsor</th>
                    <th>Last Order</th>
                    <th>Rank</th>
                    <th>Country</th>
                    <th>Status</th>
                    <th>Sales Value</th>
                    <th>Hierarchy</th>
                </tr>
            </thead>
        </table> -->

        <?php
        $conn = connDB();

        if($getWho)

        {?>
            <thead>
                <tr>
                    <th>Generation</th>
                    <th>Name</th>
                    <th>ID</th>
                    <th>Sponsor</th>
                    <th>Last Order</th>
                    <th>Rank</th>
                    <th>Country</th>
                    <th>Status</th>
                    <th>Sales Value</th>
                    <th>Hierarchy</th>

                </tr>
            </thead>
            <?php

            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                $time1 = date("d-m-Y",strtotime($thisTempUser->getDateCreated()));
                if($thisPerson->getCurrentLevel() == $lowestLevel)
                {
                    echo '<li id="'.$thisPerson->getReferralId().'">
                        <tr>'.$thisPerson->getCurrentLevel().'</tr>
                        <tr>'.$thisTempUser->getUsername().'</tr>
                        <tr>'.$thisTempUser->getMemberID().'</tr>
                        <tr>'.$thisTempUser->getUsername().'</tr>
                        <tr>'.$time1.'</tr>
                        <tr>'.$thisTempUser->getRank().'</tr>
                        <tr>'.$thisTempUser->getCountry().'</tr>
                        <tr>'.$thisTempUser->getStatus().'</tr>
                        <tr>'.$thisTempUser->getSalesValue().'</tr>
                        <tr>
                            '.'
                            <form action="userDownlineDetails.php" method="POST">
                                <button class=clean pink-button view-btn" type="submit" name="user_uid" value="'.$thisPerson->getReferralId().'">
                                    Tree View
                                </button>
                            </form>
                            '.'
                        </tr>
                        '.'
                        </li>';
                }
            }
            echo '<tr>';
            $lowestLevel = $getWho[0]->getCurrentLevel();
            foreach($getWho as $thisPerson)
            {
                $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                $thisTempUser = $tempUsers[0];
                $time2 = date("d-m-Y",strtotime($thisTempUser->getDateCreated()));
                echo '
                <script type="text/javascript">
                    var div = document.getElementById("'.$thisPerson->getReferrerId().'");
                    div.innerHTML += "<tr name=\'tr-'.$thisPerson->getReferrerId().'\'><li id=\''.$thisPerson->getReferralId().'\'>'.$thisPerson->getCurrentLevel().'&nbsp;'
                    .$thisTempUser->getUsername().'&nbsp;'.$thisTempUser->getMemberID().'&nbsp;'.$thisTempUser->getUsername().'&nbsp;'.$time2.'&nbsp;'.$thisTempUser->getRank().'&nbsp;'
                    .$thisTempUser->getCountry().'&nbsp;'.$thisTempUser->getStatus().'&nbsp;'.$thisTempUser->getSalesValue().'&nbsp;'."<form action='userDownlineDetails.php' method='post'><button type='submit' name='user_uid' value='".$thisPerson->getReferralId()."' >Tree View</button></form>".'</li></ul>";
                </script>
                ';
            }
            echo '</tr>';
            echo '</table>';
        }
        ?>

    </div>


</div>

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
